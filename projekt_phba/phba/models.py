from django.db import models
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class Video(models.Model):
    video_title = models.CharField(max_length=200,default='')
    pub_date = models.DateTimeField(auto_now_add=True, blank=True)
    video_file = models.FileField()
    video_tumbnail = models.FileField()
    video_rating = models.IntegerField(default=0)
    video_description = models.CharField(max_length=200,default='')
    video_category = models.CharField(max_length=20,default='')

@python_2_unicode_compatible
class Ad(models.Model):
    ad_title = models.CharField(max_length=200,default='')
    ad_image = models.FileField()
    ad_description = models.CharField(max_length=200,default='') 

   

