# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-05 09:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phba', '0002_video_video_da'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='video_da',
            field=models.FileField(upload_to=''),
        ),
    ]
