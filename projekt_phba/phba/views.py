from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import Video,Ad
from django.urls import reverse
from pure_pagination import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response, render, redirect
from django.contrib.auth.decorators import login_required
from .forms import UploadVideoForm


def index(request):
    try:
        page = request.GET.get('page', 1)
    except PageNotAnInteger:
        page = 1

    objects = Video.objects.all()
    # Provide Paginator with the request object for complete querystring generation

    p = Paginator(objects, request=request, per_page=2)

    videos = p.page(page)
    ad = get_object_or_404(Ad, pk=page)
    return render(request, 'phba/index.html', {
        'videos': videos,
        'ad':ad,
    })


def watch(request, video_id):
    video = get_object_or_404(Video, pk=video_id)
    return render(request,'phba/watch.html', {
        'video': video,
    })

def upload(request):
    if request.method == 'POST':
        form = UploadVideoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()

    else:
        form = UploadVideoForm()
    return render(request, 'phba/upload.html', {'form': form})



