from django import forms
from .models import Video

class UploadVideoForm(forms.ModelForm):
    video_title = forms.CharField(max_length=200)
    video_file = forms.FileField()
    video_tumbnail = forms.FileField()
    video_description = forms.CharField(max_length=200)
    video_category = forms.CharField(max_length=20)
    
    class Meta:
        model = Video
        fields = ('video_title', 'video_file', 'video_tumbnail', 'video_description', 'video_category',)
